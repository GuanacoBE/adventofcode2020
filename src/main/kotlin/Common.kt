class Pair<T>(val value1: T, val value2: T) {
    override fun toString(): String {
        return "Pair(value1=$value1, value2=$value2)"
    }
}

class Trio<T>(val a: T, val b: T, val c: T) {
    override fun toString(): String {
        return "Trio(a=$a, b=$b, c=$c)"
    }
}

fun printCharArray(table: List<Array<Char>>) {
    println("----------")
    for (i in table.indices) {
        for (j in table[0].indices) {
            print("${table[i][j]}")
        }
        println()
    }
    println("----------")
}

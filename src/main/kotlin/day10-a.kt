fun main(args: Array<String>) {
    /*val input = "28\n" +
            "33\n" +
            "18\n" +
            "42\n" +
            "31\n" +
            "14\n" +
            "46\n" +
            "20\n" +
            "48\n" +
            "47\n" +
            "24\n" +
            "23\n" +
            "49\n" +
            "45\n" +
            "19\n" +
            "38\n" +
            "39\n" +
            "11\n" +
            "1\n" +
            "32\n" +
            "25\n" +
            "35\n" +
            "8\n" +
            "17\n" +
            "7\n" +
            "9\n" +
            "4\n" +
            "2\n" +
            "34\n" +
            "10\n" +
            "3"*/

    val input = {}.javaClass.getResource("day10-input.txt").readText(Charsets.UTF_8)

    val adapters = input.split("\n").map { it.toInt() }.sorted().toMutableList()

    adapters.add(adapters.last() + 3)

    var previousAdapter = 0
    val difference = mutableMapOf<Int, Int>()

    for (adapter in adapters) {
        val tmp = adapter - previousAdapter
        val counter = difference.getOrPut(tmp) { 0 }
        difference[tmp] = counter+1
        previousAdapter = adapter
    }

    println(difference)
    println(difference[1]?.times(difference[3]!!)) // 2574


}

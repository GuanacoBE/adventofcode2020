import java.util.*

fun main(args: Array<String>) {

    /*val input = "L.LL.LL.LL\n" +
            "LLLLLLL.LL\n" +
            "L.L.L..L..\n" +
            "LLLL.LL.LL\n" +
            "L.LL.LL.LL\n" +
            "L.LLLLL.LL\n" +
            "..L.L.....\n" +
            "LLLLLLLLLL\n" +
            "L.LLLLLL.L\n" +
            "L.LLLLL.LL"*/

    val input = {}.javaClass.getResource("day11-input.txt").readText(Charsets.UTF_8)

    val seats = input.split("\n").map { it.toCharArray().toTypedArray() }

    val length = seats[0].size
    val height = seats.size

    var countSeatsChanged = 0
    var previousCount: Int

    do {
        val previousState = ArrayList(seats.map { it.copyOf() })
        previousCount = countSeatsChanged
        for (i in 0 until height) {
            for (j in 0 until length) {
                val currentSpot = previousState[i][j]
                if (currentSpot == 'L' || currentSpot == '#') {
                    val needToChange = needToChange(previousState, i, j)
                    if (needToChange) {
                        countSeatsChanged++
                        if (currentSpot == 'L') {
                            seats[i][j] = '#'
                        } else {
                            seats[i][j] = 'L'
                        }
                    }
                }
            }
        }
    } while (previousCount != countSeatsChanged)

    println(countOccupied(seats)) // 2108
}

fun countOccupied(seats: List<Array<Char>>): Long {
    return seats.stream()
        .flatMap { Arrays.stream(it) }
        .filter { it == '#' }
        .count()
}

fun needToChange(seats: List<Array<Char>>, line: Int, col: Int): Boolean {
    val isEmpty = seats[line][col] == 'L'
    var countAdjacentOccupied = 0

    // south
    if (line + 1 < seats.size) {
        if (isEmpty) {
            if (seats[line + 1][col] == '#') {
                return false
            }
        } else {
            if (seats[line + 1][col] == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // souht west
    if (line + 1 < seats.size && col - 1 >= 0) {
        if (isEmpty) {
            if (seats[line + 1][col - 1] == '#') {
                return false
            }
        } else {
            if (seats[line + 1][col - 1] == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // west
    if (col - 1 >= 0) {
        if (isEmpty) {
            if (seats[line][col - 1] == '#') {
                return false
            }
        } else {
            if (seats[line][col - 1] == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // north west
    if (col - 1 >= 0 && line - 1 >= 0) {
        if (isEmpty) {
            if (seats[line - 1][col - 1] == '#') {
                return false
            }
        } else {
            if (seats[line - 1][col - 1] == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // north
    if (line - 1 >= 0) {
        if (isEmpty) {
            if (seats[line - 1][col] == '#') {
                return false
            }
        } else {
            if (seats[line - 1][col] == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // north east
    if (line - 1 >= 0 && col + 1 < seats[0].size) {
        if (isEmpty) {
            if (seats[line - 1][col + 1] == '#') {
                return false
            }
        } else {
            if (seats[line - 1][col + 1] == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // east
    if (col + 1 < seats[0].size) {
        if (isEmpty) {
            if (seats[line][col + 1] == '#') {
                return false
            }
        } else {
            if (seats[line][col + 1] == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // south east
    if (col + 1 < seats[0].size && line + 1 < seats.size) {
        if (isEmpty) {
            if (seats[line + 1][col + 1] == '#') {
                return false
            }
        } else {
            if (seats[line + 1][col + 1] == '#') {
                countAdjacentOccupied++
            }
        }
    }

    return if (isEmpty) {
        true
    } else {
        countAdjacentOccupied >= 4
    }
}

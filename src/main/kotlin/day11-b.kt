import java.util.*

fun main(args: Array<String>) {

    /*val input = "L.LL.LL.LL\n" +
            "LLLLLLL.LL\n" +
            "L.L.L..L..\n" +
            "LLLL.LL.LL\n" +
            "L.LL.LL.LL\n" +
            "L.LLLLL.LL\n" +
            "..L.L.....\n" +
            "LLLLLLLLLL\n" +
            "L.LLLLLL.L\n" +
            "L.LLLLL.LL"*/

    val input = {}.javaClass.getResource("day11-input.txt").readText(Charsets.UTF_8)

    val seats = input.split("\n").map { it.toCharArray().toTypedArray() }

    val length = seats[0].size
    val height = seats.size

    var countSeatsChanged = 0
    var previousCount: Int

    do {
        val previousState = ArrayList(seats.map { it.copyOf() })
        previousCount = countSeatsChanged
        for (i in 0 until height) {
            for (j in 0 until length) {
                val currentSpot = previousState[i][j]
                if (currentSpot == 'L' || currentSpot == '#') {
                    val needToChange = needToChangeV2(previousState, i, j)
                    if (needToChange) {
                        countSeatsChanged++
                        if (currentSpot == 'L') {
                            seats[i][j] = '#'
                        } else {
                            seats[i][j] = 'L'
                        }
                    }
                }
            }
        }
    } while (previousCount != countSeatsChanged)

    println(countOccupied(seats)) // 1897
}

fun throughFloor(seats: List<Array<Char>>, currentLine: Int, currentCol: Int, moveLine: Int, moveCol: Int): Char {
    val nextLine = currentLine + moveLine
    val nextCol = currentCol + moveCol
    if (nextLine >= 0 && nextLine < seats.size && nextCol >= 0 && nextCol < seats[0].size) {
        return if (seats[nextLine][nextCol] == '.') {
            throughFloor(seats, nextLine, nextCol, moveLine, moveCol)
        } else {
            seats[nextLine][nextCol]
        }
    }
    return '.'
}

fun needToChangeV2(seats: List<Array<Char>>, line: Int, col: Int): Boolean {
    val isEmpty = seats[line][col] == 'L'
    var countAdjacentOccupied = 0

    // south
    if (line + 1 < seats.size) {
        val nextSouth = throughFloor(seats, line, col, 1, 0)
        if (isEmpty) {
            if (nextSouth == '#') {
                return false
            }
        } else {
            if (nextSouth == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // south west
    if (line + 1 < seats.size && col - 1 >= 0) {
        val nextSouthWest = throughFloor(seats, line, col, 1, -1)
        if (isEmpty) {
            if (nextSouthWest == '#') {
                return false
            }
        } else {
            if (nextSouthWest == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // west
    if (col - 1 >= 0) {
        val nextWest = throughFloor(seats, line, col, 0, -1)
        if (isEmpty) {
            if (nextWest == '#') {
                return false
            }
        } else {
            if (nextWest == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // north west
    if (col - 1 >= 0 && line - 1 >= 0) {
        val nextNorthWest = throughFloor(seats, line, col, -1, -1)
        if (isEmpty) {
            if (nextNorthWest == '#') {
                return false
            }
        } else {
            if (nextNorthWest == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // north
    if (line - 1 >= 0) {
        val nextNorth = throughFloor(seats, line, col, -1, 0)
        if (isEmpty) {
            if (nextNorth == '#') {
                return false
            }
        } else {
            if (nextNorth == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // north east
    if (line - 1 >= 0 && col + 1 < seats[0].size) {
        val nextNorthEast = throughFloor(seats, line, col, -1, +1)
        if (isEmpty) {
            if (nextNorthEast == '#') {
                return false
            }
        } else {
            if (nextNorthEast == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // east
    if (col + 1 < seats[0].size) {
        val nextEast = throughFloor(seats, line, col, 0, +1)
        if (isEmpty) {
            if (nextEast == '#') {
                return false
            }
        } else {
            if (nextEast == '#') {
                countAdjacentOccupied++
            }
        }
    }

    // south east
    if (col + 1 < seats[0].size && line + 1 < seats.size) {
        val southEast = throughFloor(seats, line, col, 1, +1)
        if (isEmpty) {
            if (southEast == '#') {
                return false
            }
        } else {
            if (southEast == '#') {
                countAdjacentOccupied++
            }
        }
    }

    return if (isEmpty) {
        true
    } else {
        countAdjacentOccupied >= 5
    }
}

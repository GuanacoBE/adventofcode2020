import kotlin.Pair
import kotlin.math.abs

fun main(args: Array<String>) {

    /*val input = "F10\n" +
            "N3\n" +
            "F7\n" +
            "R90\n" +
            "F11"*/

    val input = {}.javaClass.getResource("day12-input.txt").readText(Charsets.UTF_8)

    val instructions = input.split("\n").map { parseInstructionBoat(it) }

    var facing = "east"
    var y = 0  // + north  - south
    var x = 0 // + east  - west

    for (instruction in instructions) {
        when (instruction.first) {
            'N' -> y += instruction.second
            'S' -> y -= instruction.second
            'E' -> x += instruction.second
            'W' -> x -= instruction.second
            'L' -> facing = turnLeft(facing, instruction.second)
            'R' -> facing = turnRight(facing, instruction.second)
            'F' -> {
                when (facing) {
                    "east" -> x += instruction.second
                    "west" -> x -= instruction.second
                    "north" -> y += instruction.second
                    "south" -> y -= instruction.second
                }
            }
        }
    }

    println(abs(x) + abs(y)) // 1032
}

val roseDesVents = arrayOf("south", "west", "north", "east")

fun turnRight(facing: String, degree: Int): String {
    val turnsToDo = degree / 90
    val current = roseDesVents.indexOf(facing)
    val next = (current + turnsToDo) % 4
    return roseDesVents[next]
}

fun turnLeft(facing: String, degree: Int): String {
    val turnsToDo = degree / 90
    val current = roseDesVents.indexOf(facing)
    var next = (current - turnsToDo) % 4
    next = when (next) {
        -1 -> 3
        -2 -> 2
        -3 -> 1
        -4 -> 0
        else -> next
    }
    return roseDesVents[next]
}

fun parseInstructionBoat(str: String): Pair<Char, Int> {
    val command = str.first()
    val value = str.drop(1)
    return Pair(command, value.toInt())
}

import kotlin.math.abs

fun main(args: Array<String>) {

    /*val input = "F10\n" +
            "N3\n" +
            "F7\n" +
            "R90\n" +
            "F11"*/

    val input = {}.javaClass.getResource("day12-input.txt").readText(Charsets.UTF_8)

    val instructions = input.split("\n").map { parseInstructionBoat(it) }

    var y = 0  // + north  - south
    var x = 0 // + east  - west

    var waypointY = 1 // +north   -south
    var waypointX = 10 // +east   -west

    for (instruction in instructions) {
        when (instruction.first) {
            'N' -> waypointY += instruction.second
            'S' -> waypointY -= instruction.second
            'E' -> waypointX += instruction.second
            'W' -> waypointX -= instruction.second
            'L' -> {
                for (i in 0 until instruction.second / 90) {
                    val tmp = waypointY
                    waypointY = waypointX
                    waypointX = tmp * -1
                }
            }
            'R' -> {
                for (i in 0 until instruction.second / 90) {
                    val tmp = waypointY
                    waypointY = waypointX * -1
                    waypointX = tmp
                }
            }
            'F' -> {
                y += (waypointY * instruction.second)
                x += (waypointX * instruction.second)
            }
        }
    }

    println(abs(x) + abs(y)) // 156735
}

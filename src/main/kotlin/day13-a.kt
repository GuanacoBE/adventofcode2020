fun main(args: Array<String>) {

    /*val input = "939\n" +
            "7,13,x,x,59,x,31,19"*/

    val input = "1002461\n" +
            "29,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,521,x,x,x,x,x,x,x,23,x,x,x,x,13,x,x,x,17,x,x,x,x,x,x,x,x,x,x,x,x,x,601,x,x,x,x,x,37,x,x,x,x,x,x,x,x,x,x,x,x,19"

    val notes = input.split("\n")

    val eta = notes[0].toInt()
    val buses = notes[1].split(",").filter { it != "x" }.map { it.toInt() }

    println(foundBus(eta, buses)) //4207
}

fun foundBus(eta: Int, buses: List<Int>): Int {
    var count = 0
    var found = false

    while (!found) {
        if (count >= eta) {
            for (b in buses) {
                if (count % b == 0) {
                    return b * (count - eta)
                }
            }
        }
        count++
    }

    return -1
}

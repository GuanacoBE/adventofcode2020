fun main(args: Array<String>) {

    //val input = "939\n" +
    //        "7,13,x,x,59,x,31,19"

    val input = "1002461\n" +
            "29,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,521,x,x,x,x,x,x,x,23,x,x,x,x,13,x,x,x,17,x,x,x,x,x,x,x,x,x,x,x,x,x,601,x,x,x,x,x,37,x,x,x,x,x,x,x,x,x,x,x,x,19"

    val notes = input.split("\n")

    val buses = notes[1].split(",")

    val biggestId = buses.filter { it != "x" }.map { it.toInt() }.maxOrNull()!!
    val indexBiggest = buses.indexOf(biggestId.toString())

    println(biggestId)
    println(indexBiggest)

    var count = 500000000000000L
    var found = false

    while (!found) {
        if (count % biggestId == 0L) {
            found = true
        }
        count++
    }
    count--
    found = false

    var begin = System.nanoTime()

    while (!found) {
        found = true
        for (i in buses.indices) {
            if (buses[i] != "x" && (count - indexBiggest + i) % buses[i].toLong() != 0L) {
                found = false
                break
            }
        }
        if ((System.nanoTime() - begin) >= 300000000000L) {
            begin = System.nanoTime()
            println(count)
        }
        count += biggestId
    }

    println(count - biggestId - indexBiggest) // 725850285300475  11h 5m 13sec
}

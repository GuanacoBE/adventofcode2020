import kotlin.math.pow

fun main(args: Array<String>) {

    /*val input = "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\n" +
            "mem[8] = 11\n" +
            "mem[7] = 101\n" +
            "mem[8] = 0"*/

    val input = {}.javaClass.getResource("day14-input.txt").readText(Charsets.UTF_8)

    val instructions = input.split("\n")

    val memory = mutableMapOf<String, Long>()
    var currentMask = ""

    for (instruction in instructions) {
        val instructionInput = instruction.split(" = ")
        if (instructionInput[0].startsWith("mask")) {
            currentMask = instructionInput[1]
        } else {
            val index = instructionInput[0].drop(4).dropLast(1)
            val binaryInput = toBinary(instructionInput[1].toLong())
            val valueMasked = applyMask(currentMask, binaryInput)
            val decimalNumber = toDecimal(valueMasked)
            memory[index] = decimalNumber
        }
    }

    var sum = 0L

    memory.forEach { (k, v) -> sum += v }

    println(sum)

}

fun toBinary(decimalNumber: Long, binaryString: String = ""): String {
    while (decimalNumber > 0) {
        val temp = "${binaryString}${decimalNumber % 2}"
        return toBinary(decimalNumber / 2, temp)
    }
    return binaryString.reversed()
}

fun toDecimal(binaryNumber: String): Long {
    var sum = 0L
    binaryNumber.reversed().forEachIndexed { k, v ->
        sum += v.toString().toLong() * 2.0.pow(k.toDouble()).toLong()
    }
    return sum
}

fun applyMask(mask: String, number: String): String {
    val numberWithZero = number.padStart(36, '0')
    var result = ""
    for (i in mask.indices) {
        when (mask[i]) {
            'X' -> result += numberWithZero[i]
            '0' -> result += '0'
            '1' -> result += '1'
        }
    }
    return result
}

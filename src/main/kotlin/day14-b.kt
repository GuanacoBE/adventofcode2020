fun main(args: Array<String>) {

    //val input = "mask = 000000000000000000000000000000X1001X\n" +
    //      "mem[42] = 100\n" +
    //      "mask = 00000000000000000000000000000000X0XX\n" +
    //      "mem[26] = 1"

    val input = {}.javaClass.getResource("day14-input.txt").readText(Charsets.UTF_8)

    val instructions = input.split("\n")

    val memory = mutableMapOf<String, Long>()
    var currentMask = ""

    for (instruction in instructions) {
        val instructionInput = instruction.split(" = ")
        if (instructionInput[0].startsWith("mask")) {
            currentMask = instructionInput[1]
        } else {
            val index = instructionInput[0].drop(4).dropLast(1)
            val inputValue = instructionInput[1].toLong()

            val listBinaryIndexes = applyMaskIndex(currentMask, toBinary(index.toLong()))
            listBinaryIndexes.forEach { memory[it] = inputValue }
        }
    }

    var sum = 0L

    memory.forEach { (k, v) -> sum += v }

    println(sum)

}

var combinaisons = mutableListOf<String>()

fun generateBinaryCombinaison(str: Array<Char>, index: Int) {
    if (index == str.size) {
        combinaisons.add(String(str.toCharArray()))
        return
    }

    if (str[index] == 'X') {
        str[index] = '0'
        generateBinaryCombinaison(str, index + 1)

        str[index] = '1'
        generateBinaryCombinaison(str, index + 1)

        str[index] = 'X'
    } else {
        generateBinaryCombinaison(str, index + 1)
    }
}

fun applyMaskIndex(mask: String, number: String): List<String> {
    val numberWithZero = number.padStart(36, '0')
    var tmp = ""
    for (i in mask.indices) {
        when (mask[i]) {
            'X' -> tmp += 'X'
            '0' -> tmp += numberWithZero[i]
            '1' -> tmp += '1'
        }
    }

    combinaisons = mutableListOf()
    generateBinaryCombinaison(tmp.toCharArray().toTypedArray(), 0)

    return combinaisons
}
fun main(args: Array<String>) {

    // 0,3,6
    // 1,3,2
    // 2,1,3
    // 1,2,3
    // 3,1,2

    val input = mutableListOf(11, 18, 0, 20, 1, 7, 16)
    var turn = input.size

    val cache = mutableMapOf<Int, MutableList<Int>>()

    input.forEachIndexed { index, v ->
        cache.computeIfAbsent(v) {
            mutableListOf()
        }
        cache[v]?.add(index)
    }

    var spokenNumber = input.last()

    while (turn <= (30000000 - 1)) {
        spokenNumber = if (alreadySpoken(spokenNumber, cache)) {
            0
        } else {
            val indexes = cache[spokenNumber]!!
            indexes.last() - indexes[indexes.size - 2]
        }

        cache.computeIfAbsent(spokenNumber) {
            mutableListOf()
        }
        cache[spokenNumber]?.add(turn)

        turn++
    }

    println("Turn $turn = $spokenNumber") // 266
}

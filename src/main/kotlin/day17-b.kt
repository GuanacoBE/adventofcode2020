fun main(args: Array<String>) {

    //val input = ".#.\n" +
    //        "..#\n" +
    //        "###"
    val input = "##......\n" +
            ".##...#.\n" +
            ".#######\n" +
            "..###.##\n" +
            ".#.###..\n" +
            "..#.####\n" +
            "##.####.\n" +
            "##..#.##"
    val cubes = input.split("\n")

    var map = mutableMapOf<String, Boolean>()

    cubes.forEachIndexed { y, line ->
        line.forEachIndexed { x, ch ->
            if (ch == '#') {
                map["$x,$y,0,0"] = true
            }
        }
    }

    val height = mutableListOf(0, cubes.size)
    val width = mutableListOf(0, cubes[0].length)
    val depth = mutableListOf(0, 1)
    val hyper = mutableListOf(0, 1)

    for (i in 0..5) {
        val newMap = mutableMapOf<String, Boolean>()
        depth[0]--
        depth[1]++
        width[0]--
        width[1]++
        height[0]--
        height[1]++
        hyper[0]--
        hyper[1]++

        for (w in hyper[0]..hyper[1]) {
            for (z in depth[0]..depth[1]) {
                for (y in width[0]..width[1]) {
                    for (x in height[0]..height[1]) {
                        val neighbors = countAround(x, y, z, w, map)
                        val isActive = map["$x,$y,$z,$w"] == true
                        if (neighbors == 3 || (neighbors == 2 && isActive)) {
                            newMap["$x,$y,$z,$w"] = true
                        }
                    }
                }
            }
        }

        map = newMap
    }

    println(map.size) // 2572

}

fun countAround(x: Int, y: Int, z: Int, w: Int, map: Map<String, Boolean>): Int {
    var count = 0
    for (ww in (w - 1)..(w + 1)) {
        for (zz in (z - 1)..(z + 1)) {
            for (yy in (y - 1)..(y + 1)) {
                for (xx in (x - 1)..(x + 1)) {
                    if ((xx != x || yy != y || zz != z || ww != w) && map["$xx,$yy,$zz,$ww"] == true) {
                        count++
                    }
                }
            }
        }
    }
    return count
}

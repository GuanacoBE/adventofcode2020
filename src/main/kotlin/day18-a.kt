fun main(args: Array<String>) {

    //val input = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"
    val input = {}.javaClass.getResource("day18-input.txt").readText(Charsets.UTF_8)

    val expressions = input.split("\n")

    var result = 0L

    val regexFirstParenthesis = """\([^()]+\)""".toRegex()

    for (expression in expressions) {
        var line = expression
        var tmp = regexFirstParenthesis.find(line)
        while (tmp != null) {
            val parenthesisResult = compute(tmp.value.drop(1).dropLast(1))
            line = regexFirstParenthesis.replaceFirst(line, parenthesisResult.toString())
            tmp = regexFirstParenthesis.find(line)
        }
        result += compute(line)
    }


    println(result) // 8298263963837

}

fun compute(expression: String): Long {
    val operands = expression.split(' ')
    var result: Long = operands[0].toLong()

    for (i in 1 until operands.size step 2) {
        when (operands[i]) {
            "+" -> result += operands[i + 1].toLong()
            "*" -> result *= operands[i + 1].toLong()
        }
    }

    return result
}

fun main(args: Array<String>) {

    //val input = "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"
    val input = {}.javaClass.getResource("day18-input.txt").readText(Charsets.UTF_8)

    val expressions = input.split("\n")

    var result = 0L

    val regexFirstParenthesis = """\([^()]+\)""".toRegex()

    for (expression in expressions) {
        var line = expression
        var tmp = regexFirstParenthesis.find(line)
        while (tmp != null) {
            val parenthesisResult = compute2(tmp.value.drop(1).dropLast(1))
            line = regexFirstParenthesis.replaceFirst(line, parenthesisResult.toString())
            tmp = regexFirstParenthesis.find(line)
        }
        result += compute2(line)
    }

    println(result) // 145575710203332
}

val regexPlusOperation = """\d+\s\+\s\d+""".toRegex()

fun compute2(line: String): Long {
    var expression = line
    var tmp = regexPlusOperation.find(expression)
    while (tmp != null) {
        val sumOperation = compute(tmp.value)
        expression = regexPlusOperation.replaceFirst(expression, sumOperation.toString())
        tmp = regexPlusOperation.find(expression)
    }
    return compute(expression)
}

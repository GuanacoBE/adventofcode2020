fun main(args: Array<String>) {

    /*val inputRules = "0: 4 1 5\n" +
            "1: 2 3 | 3 2\n" +
            "2: 4 4 | 5 5\n" +
            "3: 4 5 | 5 4\n" +
            "4: \"a\"\n" +
            "5: \"b\""

    val inputMessages = "ababbb\n" +
            "bababa\n" +
            "abbbab\n" +
            "aaabbb\n" +
            "aaaabbb"*/

    val input = {}.javaClass.getResource("day19-input.txt").readText(Charsets.UTF_8)

    val inputSplit = input.split("\n\n")
    val inputRules = inputSplit[0]
    val inputMessages = inputSplit[1]

    val messages = inputMessages.split("\n")

    val rules = parseRules(inputRules)
    val pattern = "(${rules["0"]!!})"
    val regex = Regex(pattern)

    println(messages.count { regex.matches(it) }) // 291

}

fun parseRules(rules: String): Map<String, String> {
    val result = mutableMapOf<String, String>()

    rules.split("\n").forEach {
        val ruleSplit = it.split(":")
        val key = ruleSplit[0]
        val trimmed = ruleSplit[1].trim()
        val indexOf = trimmed.indexOf("|")
        val cleaned = if (indexOf == -1) {
            trimmed
        } else {
            "( ${trimmed.substring(0, indexOf - 1)} ) | ( ${trimmed.subSequence(indexOf + 2, trimmed.length)} )"
        }
        result[key] = cleaned
    }

    while (true) {
        val (key, toReplace) = result.entries.find { hasNumbers(it.value) } ?: break
        val parts = toReplace.split(" ")
        val newValue = mutableListOf<String>()
        parts.forEach { part ->
            newValue.add(
                when {
                    part == "|" -> "|"
                    part == "(" -> "("
                    part == ")" -> ")"
                    part == "+" -> "+"
                    part.startsWith("\"") -> part
                    else -> "( ${result[part]!!} )"
                }
            )
        }
        val newJoinedValue = newValue.joinToString(separator = " ")
        result[key] = newJoinedValue
    }

    for (key in result.keys) {
        result[key] = result[key]!!
            .replace(" ", "")
            .replace("\"", "")
    }
    return result
}

fun hasNumbers(rule: String) = Regex(".*[0-9]+.*").matches(rule)
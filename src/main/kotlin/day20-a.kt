import java.util.stream.Collectors

fun main(args: Array<String>) {

    val input = {}.javaClass.getResource("day20-input.txt").readText(Charsets.UTF_8)

    val tiles = input.split("\n\n")
        .map { t ->
            val titleData = t.split(":\n")
            titleData[0].drop(5) to titleData[1].split("\n")
        }.toMap()

    //println(tiles)

    val matched = mutableSetOf<String>()

    for ((key, tileA) in tiles) {
        for ((keyInner, tileB) in tiles) {
            if (key != keyInner) {
                if (compareTile(tileA, tileB)) {
                    if (!matched.contains("$key->$keyInner") && !matched.contains("$keyInner->$key")) {
                        matched.add("$key->$keyInner")
                    }
                }
            }
        }
    }

    var total = 1L

    for (k in tiles.keys) {
        val appears = matched.count { it.contains(k) }
        if (appears == 2) {
            println(k)
            total *= k.toLong()
        }
    }

    println(total) // 84116744709593

}

fun compareTile(tileA: List<String>, tileB: List<String>): Boolean {
    // south
    if (compareSide(tileA.last(), tileB.last())) {
        return true
    }
    if (compareSide(tileA.last(), tileB.first())) {
        return true
    }

    if (compareSide(tileA.last(), getSide(tileB, 0))) {
        return true
    }

    if (compareSide(tileA.last(), getSide(tileB, 9))) {
        return true
    }

    // north
    if (compareSide(tileA.first(), tileB.last())) {
        return true
    }
    if (compareSide(tileA.first(), tileB.first())) {
        return true
    }

    if (compareSide(tileA.first(), getSide(tileB, 0))) {
        return true
    }

    if (compareSide(tileA.first(), getSide(tileB, 9))) {
        return true
    }

    // west
    if (compareSide(getSide(tileA, 0), tileB.last())) {
        return true
    }
    if (compareSide(getSide(tileA, 0), tileB.first())) {
        return true
    }

    if (compareSide(getSide(tileA, 0), getSide(tileB, 0))) {
        return true
    }

    if (compareSide(getSide(tileA, 0), getSide(tileB, 9))) {
        return true
    }

    // east
    if (compareSide(getSide(tileA, 9), tileB.last())) {
        return true
    }
    if (compareSide(getSide(tileA, 9), tileB.first())) {
        return true
    }

    if (compareSide(getSide(tileA, 9), getSide(tileB, 0))) {
        return true
    }

    if (compareSide(getSide(tileA, 9), getSide(tileB, 9))) {
        return true
    }

    return false
}

fun getSide(tile: List<String>, side: Int): String {
    return tile.stream()
        .map { it[side] }
        .collect(Collectors.toList())
        .joinToString(separator = "")
}

fun compareSide(sideA: String, sideB: String): Boolean {
    return sideA == sideB || sideA == sideB.reversed()
}

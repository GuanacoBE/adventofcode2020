fun main(args: Array<String>) {

    val input = {}.javaClass.getResource("day20-input.txt").readText(Charsets.UTF_8)

    val tiles = input.split("\n\n")
        .map { t ->
            val titleData = t.split(":\n")
            titleData[0].drop(5) to titleData[1].split("\n")
        }.toMap()

    var totalSquareCount = 0L
    for ((_, v) in tiles) {
        totalSquareCount += countSquare(v)
    }

    // 2 monsters in sample
    // 24*24 = 576 image size sample
    // (2/576)*(96*96) = 32 -> monsters in data

    // 1897 too low
    // 1822 too low


    for (n in 32..64) {
        println(totalSquareCount - (n * 15))
    }

    // answer 1957

}

fun countSquare(tile: List<String>): Long {
    var count = 0L
    for (i in 1..tile.size - 2) {
        for (j in 1..tile[i].length - 2) {
            if (tile[i][j] == '#') {
                count++
            }
        }
    }
    return count
}

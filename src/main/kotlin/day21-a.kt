import java.util.stream.Collectors

fun main(args: Array<String>) {

    /*val input = """
         mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
         trh fvjkl sbzzf mxmxvkd (contains dairy)
         sqjhc fvjkl (contains soy)
         sqjhc mxmxvkd sbzzf (contains fish)
     """.trimIndent()*/
    val input = {}.javaClass.getResource("day21-input.txt").readText(Charsets.UTF_8)

    val foods = input.split("\n")

    val mapFoods = mutableMapOf<String, List<String>>()
    for (f in foods) {
        val split = f.split(" (contains")
        val ingredients = split[0].trim()
        val allergens =
            if (split[1].last() == ')') split[1].dropLast(1).trim().split(", ") else split[1].trim().split(", ")
        mapFoods[ingredients] = allergens
    }

    val mapAllergens = mutableMapOf<String, Set<String>>()
    for ((k, v) in mapFoods) {
        for (allergens in v) {
            var ingredients = k.split(" ").toSet()
            for ((kInner, vInner) in mapFoods) {
                if (k != kInner && vInner.contains(allergens)) {
                    ingredients = ingredients.intersect(kInner.split(" "))
                }
            }
            mapAllergens[allergens] = ingredients
        }
    }

    println(mapAllergens)

    val allIngredients = mapFoods.keys.stream()
        .flatMap { it.split(" ").stream() }
        .collect(Collectors.toSet())

    val allAllergensIngredients = mapAllergens.values.stream()
        .flatMap { it.stream() }
        .collect(Collectors.toSet())

    val noAllergenFood = allIngredients.subtract(allAllergensIngredients)

    // kfcds, nhms, sbzzf, trh
    println(noAllergenFood)

    var count = 0
    noAllergenFood.forEach {
        for (k in mapFoods.keys) {
            var tmpCount = 0
            k.split(" ").forEach { w ->
                if (it == w) {
                    tmpCount++
                }
            }
            count += tmpCount
        }
    }

    println(count) // 2826


}

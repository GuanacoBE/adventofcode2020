fun main(args: Array<String>) {

    /*val input = """
         mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
         trh fvjkl sbzzf mxmxvkd (contains dairy)
         sqjhc fvjkl (contains soy)
         sqjhc mxmxvkd sbzzf (contains fish)
     """.trimIndent()*/
    val input = {}.javaClass.getResource("day21-input.txt").readText(Charsets.UTF_8)

    val foods = input.split("\n")

    val mapFoods = mutableMapOf<String, List<String>>()
    for (f in foods) {
        val split = f.split(" (contains")
        val ingredients = split[0].trim()
        val allergens =
            if (split[1].last() == ')') split[1].dropLast(1).trim().split(", ") else split[1].trim().split(", ")
        mapFoods[ingredients] = allergens
    }

    val mapAllergens = mutableMapOf<String, Set<String>>()
    for ((k, v) in mapFoods) {
        for (allergens in v) {
            var ingredients = k.split(" ").toSet()
            for ((kInner, vInner) in mapFoods) {
                if (k != kInner && vInner.contains(allergens)) {
                    ingredients = ingredients.intersect(kInner.split(" "))
                }
            }
            mapAllergens[allergens] = ingredients
        }
    }

    println(mapAllergens.toSortedMap())

    val dangerous = mutableListOf<String>()

    for ((k, v) in mapAllergens.toSortedMap()) {
        for (i in v) {
            if (!dangerous.contains(i) && !(k == "eggs" && i == "xzb")) {
                dangerous.add(i)
            }
        }
    }

    // NOT : lkdg,pbhthx,xzb,sqdsxhb,dnlsjr,dgvqv,rsvlb,csnfnl
    // NOT : lkdg,pbhthx,sqdsxhb,dnlsjr,dgvqv,rsvlb,csnfnl,xzb
    // NOT : lkdg,pbhthx,xzb,sqdsxhb,dnlsjr,dgvqv,rsvlb,sqdsxhb,csnfnl,dnlsjr,sqdsxhb,dnlsjr,xzb,rsvlb,lkdg,csnfnl,dgvqv,dnlsjr,rsvlb

    // pbhthx,sqdsxhb,dgvqv,csnfnl,dnlsjr,xzb,lkdg,rsvlb

    // dairy = pbhthx
    // eggs = sqdsxhb
    // fish = dgvqv
    // nuts = csnfnl
    // sesame = dnlsjr
    // shellfish = xzb
    // soy = lkdg
    // weat = rsvlb

    println(dangerous.joinToString(separator = ","))


}

import java.util.*

fun main(args: Array<String>) {

    //val player1: Queue<Int> = LinkedList(listOf(9,2,6,3,1))
    //val player2: Queue<Int> = LinkedList(listOf(5,8,4,7,10))

    val player1: Queue<Int> =
        LinkedList(listOf(14, 23, 6, 16, 46, 24, 13, 25, 17, 4, 31, 7, 1, 47, 15, 9, 50, 3, 30, 37, 43, 10, 28, 33, 32))
    val player2: Queue<Int> = LinkedList(
        listOf(
            29,
            49,
            11,
            42,
            35,
            18,
            39,
            40,
            36,
            19,
            48,
            22,
            2,
            20,
            26,
            8,
            12,
            44,
            45,
            21,
            38,
            41,
            34,
            5,
            27
        )
    )

    while (!(player1.isEmpty() || player2.isEmpty())) {
        val player1TopCard = player1.remove()
        val player2TopCard = player2.remove()

        if (player1TopCard > player2TopCard) {
            player1.add(player1TopCard)
            player1.add(player2TopCard)
        } else {
            player2.add(player2TopCard)
            player2.add(player1TopCard)
        }

    }

    println(player1)
    println(player2)

    val winner = if (player1.isEmpty()) {
        player2
    } else {
        player1
    }

    var numberOfCard = winner.size

    var total = 0L

    while (winner.isNotEmpty()) {
        val card = winner.remove()
        total += (card * numberOfCard)
        numberOfCard--
    }

    println(total) // 31754

}

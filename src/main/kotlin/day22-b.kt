import java.util.*

fun main(args: Array<String>) {

    //val player1: Queue<Int> = LinkedList(listOf(9, 2, 6, 3, 1))
    //val player2: Queue<Int> = LinkedList(listOf(5, 8, 4, 7, 10))

    val player1: Queue<Int> =
        LinkedList(listOf(14, 23, 6, 16, 46, 24, 13, 25, 17, 4, 31, 7, 1, 47, 15, 9, 50, 3, 30, 37, 43, 10, 28, 33, 32))
    val player2: Queue<Int> = LinkedList(
        listOf(
            29,
            49,
            11,
            42,
            35,
            18,
            39,
            40,
            36,
            19,
            48,
            22,
            2,
            20,
            26,
            8,
            12,
            44,
            45,
            21,
            38,
            41,
            34,
            5,
            27
        )
    )

    playAGame(player1, player2)

    println(player1)
    println(player2)

    computeScore(if (player1.isEmpty()) player2 else player1) // 35436

}

private fun playAGame(player1: Queue<Int>, player2: Queue<Int>): Boolean {
    val alreadyPlayedGames = mutableSetOf<String>()
    while (!(player1.isEmpty() || player2.isEmpty())) {
        if (alreadyPlayedGames.contains(player1.toString() + player2.toString())) {
            player1.addAll(player2)
            player2.clear()
        } else {
            alreadyPlayedGames.add(player1.toString() + player2.toString())
            val player1TopCard = player1.remove()
            val player2TopCard = player2.remove()

            if (player1.size >= player1TopCard && player2.size >= player2TopCard) {
                val player1Copy: Queue<Int> = LinkedList()
                val player1Iterator = player1.iterator()
                var numberCardToTakePlayer1 = player1TopCard
                while (numberCardToTakePlayer1 > 0 && player1Iterator.hasNext()) {
                    player1Copy.add(player1Iterator.next())
                    numberCardToTakePlayer1--
                }

                val player2Copy: Queue<Int> = LinkedList()
                val player2Iterator = player2.iterator()
                var numberCardToTakePlayer2 = player2TopCard
                while (numberCardToTakePlayer2 > 0 && player2Iterator.hasNext()) {
                    player2Copy.add(player2Iterator.next())
                    numberCardToTakePlayer2--
                }

                val player1Winner = playAGame(player1Copy, player2Copy)
                if (player1Winner) {
                    player1.add(player1TopCard)
                    player1.add(player2TopCard)
                } else {
                    player2.add(player2TopCard)
                    player2.add(player1TopCard)
                }
            } else {
                if (player1TopCard > player2TopCard) {
                    player1.add(player1TopCard)
                    player1.add(player2TopCard)
                } else {
                    player2.add(player2TopCard)
                    player2.add(player1TopCard)
                }
            }
        }
    }
    return !player1.isEmpty()
}

private fun computeScore(winner: Queue<Int>) {
    var numberOfCard = winner.size
    var total = 0L
    while (winner.isNotEmpty()) {
        val card = winner.remove()
        total += (card * numberOfCard)
        numberOfCard--
    }
    println(total)
}

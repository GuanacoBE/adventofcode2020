fun main(args: Array<String>) {
    val cups = "459672813".map { Node(it.toString().toInt(), null) }
    for (i in cups.indices) {
        cups[i].next = cups[(i + 1) % cups.size]
    }

    var currentNode: Node? = cups[0]
    for (i in 1..100) {
        val toMoveNode = currentNode?.next
        currentNode?.next = currentNode?.next?.next?.next?.next

        var destinationValue = if (currentNode?.value == 1) 9 else currentNode!!.value - 1
        while (destinationValue in listOf(toMoveNode?.value, toMoveNode?.next?.value, toMoveNode?.next?.next?.value)) {
            destinationValue -= 1
            if (destinationValue < 1) {
                destinationValue = 9
            }
        }

        var destinationNode = currentNode.next
        while (destinationNode?.value != destinationValue) {
            destinationNode = destinationNode?.next
        }

        toMoveNode?.next?.next?.next = destinationNode.next
        destinationNode.next = toMoveNode
        currentNode = currentNode.next
    }

    var nodeValue1 = currentNode
    while (nodeValue1?.value != 1) {
        nodeValue1 = nodeValue1?.next
    }
    while (nodeValue1?.next?.value != 1) {
        print(nodeValue1?.next?.value)
        nodeValue1 = nodeValue1?.next
    }
    // 68245739
}

data class Node(val value: Int, var next: Node?)
fun main(args: Array<String>) {
    val cups = "459672813".map { Node(it.toString().toInt(), null) }
    for (i in cups.indices) {
        cups[i].next = cups[(i + 1) % cups.size]
    }
    var currentNode: Node = cups[8]
    for (i in 10..1000000) {
        currentNode.next = Node(i, null)
        currentNode = currentNode.next!!
    }
    currentNode.next = cups[0]
    currentNode = currentNode.next!!
    var mapCups = mutableMapOf(currentNode.value to currentNode)
    var tmp = currentNode.next!!
    while (tmp != currentNode) {
        mapCups[tmp.value] = tmp
        tmp = tmp.next!!
    }

    for (i in 1..10000000) {
        val move = currentNode.next
        currentNode.next = currentNode.next?.next?.next?.next

        var destinationValue = if (currentNode.value == 1) 1000000 else currentNode.value - 1
        while (destinationValue in listOf(move?.value, move?.next?.value, move?.next?.next?.value)) {
            destinationValue -= 1
            if (destinationValue < 1) {
                destinationValue = 1000000
            }
        }

        var destinationNode = mapCups[destinationValue]

        move?.next?.next?.next = destinationNode!!.next
        destinationNode.next = move
        currentNode = currentNode.next!!
    }

    var nodeValue1 = mapCups[1]!!
    println("${nodeValue1.next!!.value.toLong() * nodeValue1.next!!.next!!.value.toLong()}") // 219634632000
}
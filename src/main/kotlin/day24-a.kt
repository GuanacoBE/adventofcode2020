fun main(args: Array<String>) {

    /*val input = """sesenwnenenewseeswwswswwnenewsewsw
                    neeenesenwnwwswnenewnwwsewnenwseswesw
                    seswneswswsenwwnwse
                    nwnwneseeswswnenewneswwnewseswneseene
                    swweswneswnenwsewnwneneseenw
                    eesenwseswswnenwswnwnwsewwnwsene
                    sewnenenenesenwsewnenwwwse
                    wenwwweseeeweswwwnwwe
                    wsweesenenewnwwnwsenewsenwwsesesenwne
                    neeswseenwwswnwswswnw
                    nenwswwsewswnenenewsenwsenwnesesenew
                    enewnwewneswsewnwswenweswnenwsenwsw
                    sweneswneswneneenwnewenewwneswswnese
                    swwesenesewenwneswnwwneseswwne
                    enesenwswwswneneswsenwnewswseenwsese
                    wnwnesenesenenwwnenwsewesewsesesew
                    nenewswnwewswnenesenwnesewesw
                    eneswnwswnwsenenwnwnwwseeswneewsenese
                    neswnwewnwnwseenwseesewsenwsweewe
                    wseweeenwnesenwwwswnew""".trimIndent()*/

    val input = {}.javaClass.getResource("day24-input.txt").readText(Charsets.UTF_8)

    val tilesToFlip = input.split("\n")

    val counts = mutableMapOf<Point, Int>()

    tilesToFlip.map { it.trim() }.forEach { line ->
        var x = 0
        var y = 0
        var i = 0
        while (i < line.length) {
            var c = line[i].toString()
            if (c == "n" || c == "s") {
                c += line[i + 1]
                i++
            }
            // e, se, sw, w, nw, and ne
            val direction = HexaDirection.valueOf(c.toUpperCase())
            x += direction.x
            y += direction.y
            i++
        }

        counts.merge(Point(x, y), 1) { o: Int, n: Int ->
            o + n
        }

    }


    var totalBlack = 0
    for ((_, v) in counts) {
        if (v % 2 == 1) {
            totalBlack++
        }
    }

    println(totalBlack) // 455

}

data class Point(val x: Int, val y: Int)

enum class HexaDirection(val x: Int, val y: Int) {
    E(0, 1),
    W(0, -1),
    NE(1, 0),
    NW(1, -1),
    SE(-1, 1),
    SW(-1, 0)
}

fun main(args: Array<String>) {

    /*val input = """sesenwnenenewseeswwswswwnenewsewsw
                    neeenesenwnwwswnenewnwwsewnenwseswesw
                    seswneswswsenwwnwse
                    nwnwneseeswswnenewneswwnewseswneseene
                    swweswneswnenwsewnwneneseenw
                    eesenwseswswnenwswnwnwsewwnwsene
                    sewnenenenesenwsewnenwwwse
                    wenwwweseeeweswwwnwwe
                    wsweesenenewnwwnwsenewsenwwsesesenwne
                    neeswseenwwswnwswswnw
                    nenwswwsewswnenenewsenwsenwnesesenew
                    enewnwewneswsewnwswenweswnenwsenwsw
                    sweneswneswneneenwnewenewwneswswnese
                    swwesenesewenwneswnwwneseswwne
                    enesenwswwswneneswsenwnewswseenwsese
                    wnwnesenesenenwwnenwsewesewsesesew
                    nenewswnwewswnenesenwnesewesw
                    eneswnwswnwsenenwnwnwwseeswneewsenese
                    neswnwewnwnwseenwseesewsenwsweewe
                    wseweeenwnesenwwwswnew""".trimIndent()  */

    val input = {}.javaClass.getResource("day24-input.txt").readText(Charsets.UTF_8)

    val tilesToFlip = input.split("\n")

    val counts = mutableMapOf<Point, Int>()

    tilesToFlip.map { it.trim() }.forEach { line ->
        var x = 0
        var y = 0
        var i = 0
        while (i < line.length) {
            var c = line[i].toString()
            if (c == "n" || c == "s") {
                c += line[i + 1]
                i++
            }
            // e, se, sw, w, nw, and ne
            val direction = HexaDirection.valueOf(c.toUpperCase())
            x += direction.x
            y += direction.y
            i++
        }

        counts.merge(Point(x, y), 1) { o: Int, n: Int ->
            o + n
        }

    }


    var blackTiles = hashSetOf<Point>()
    for ((k, v) in counts) {
        if (v % 2 == 1) {
            blackTiles.add(k)
        }
    }

    repeat(100) {
        val dayCounts = blackTiles.map { it to 0 }.toMap().toMutableMap()

        blackTiles.forEach { bt ->
            for (d in HexaDirection.values()) {
                dayCounts.merge(Point(bt.x + d.x, bt.y + d.y), 1) { o: Int, n: Int ->
                    o + n
                }
            }
        }

        val newBlackTiles = hashSetOf<Point>()

        for ((k, v) in dayCounts) {
            if (blackTiles.contains(k)) {
                if (v == 1 || v == 2) {
                    newBlackTiles.add(k)
                }
            } else {
                if (v == 2) {
                    newBlackTiles.add(k)
                }
            }
        }

        blackTiles = newBlackTiles


    }

    println(blackTiles.size)

}

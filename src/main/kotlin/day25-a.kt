fun main(args: Array<String>) {

    val initSubject = 7

    val inputDoor = 2084668L
    var i = 1L
    var loopSize = 0
    while (i != inputDoor) {
        i *= initSubject
        i %= 20201227
        loopSize++
    }
    println(loopSize)

    val inputShip = 3704642L
    var j = 1L
    var loopSizeShip = 0
    while (j != inputShip) {
        j *= initSubject
        j %= 20201227
        loopSizeShip++
    }
    println(loopSizeShip)


    var encrKey = 1L
    repeat(loopSizeShip) {
        encrKey *= inputDoor
        encrKey %= 20201227
    }

    println(encrKey)

}

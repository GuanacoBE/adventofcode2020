fun main(args: Array<String>) {
    /*val input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\n" +
            "byr:1937 iyr:2017 cid:147 hgt:183cm\n" +
            "\n" +
            "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n" +
            "hcl:#cfa07d byr:1929\n" +
            "\n" +
            "hcl:#ae17e1 iyr:2013\n" +
            "eyr:2024\n" +
            "ecl:brn pid:760753108 byr:1931\n" +
            "hgt:179cm\n" +
            "\n" +
            "hcl:#cfa07d eyr:2025 pid:166559648\n" +
            "iyr:2011 ecl:brn hgt:59in"*/

    val input = {}.javaClass.getResource("day4-input.txt").readText(Charsets.UTF_8)

    val passports = parsePassport(input)

    var validPassport = passports.stream()
        .map { isValid(it) }
        .filter { it == true}
        .count()

    println(validPassport)
}

fun isValid(passport: Map<String, String>): Boolean {
    val mandatory = listOf("byr","iyr","eyr","hgt","hcl","ecl","pid")
    for (f in mandatory) {
        if (!passport.containsKey(f)) {
            return false
        }
    }
    return true
}

fun parsePassport(input: String): List<Map<String, String>> {
    val passPortData = input.split("\n\n")

    val result = emptyList<Map<String, String>>().toMutableList()

    for (data in passPortData) {
        val passport = emptyMap<String, String>().toMutableMap()
        val dataCleaned = data.replace("\n", " ")
        val attributes = dataCleaned.split(" ")

        for (attribute in attributes) {
            val attributeData = attribute.split(":")
            passport[attributeData[0]] = attributeData[1]
        }

        result.add(passport)
    }

    return result
}
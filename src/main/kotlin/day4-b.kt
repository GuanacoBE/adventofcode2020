fun main(args: Array<String>) {
    /*val input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\n" +
            "byr:1937 iyr:2017 cid:147 hgt:183cm\n" +
            "\n" +
            "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n" +
            "hcl:#cfa07d byr:1929\n" +
            "\n" +
            "hcl:#ae17e1 iyr:2013\n" +
            "eyr:2024\n" +
            "ecl:brn pid:760753108 byr:1931\n" +
            "hgt:179cm\n" +
            "\n" +
            "hcl:#cfa07d eyr:2025 pid:166559648\n" +
            "iyr:2011 ecl:brn hgt:59in"*/

    val input = {}.javaClass.getResource("day4-input.txt").readText(Charsets.UTF_8)

    val passports = parsePassport(input)

    var validPassport = passports.stream()
        .map { isValidAndValidated(it) }
        .filter { it == true}
        .count()

    println(validPassport)
}

fun isValidAndValidated(passport: Map<String, String>): Boolean {
    val mandatory = listOf("byr","iyr","eyr","hgt","hcl","ecl","pid")
    for (f in mandatory) {
        if (!passport.containsKey(f)) {
            return false
        }
        val isFieldValid = when(f) {
            "byr" -> validate4DigitsAndDate(passport[f].toString(), 1920, 2002)
            "iyr" -> validate4DigitsAndDate(passport[f].toString(), 2010, 2020)
            "eyr" -> validate4DigitsAndDate(passport[f].toString(), 2020, 2030)
            "hgt" -> validateHgt(passport[f].toString())
            "hcl" -> "^#([a-f0-9]{6})$".toRegex().matches(passport[f].toString())
            "ecl" -> listOf("amb","blu","brn","gry","grn","hzl","oth").contains(passport[f].toString())
            "pid" -> """\d{9}""".toRegex().matches(passport[f].toString())
            else -> false
        }
        if(!isFieldValid) {
            return false
        }
    }
    return true
}

fun validateHgt(input: String) : Boolean {
    return when {
        input.endsWith("cm") -> input.dropLast(2).toInt() in 150..193
        input.endsWith("in") -> input.dropLast(2).toInt() in 59..76
        else -> false
    }
}

fun validate4DigitsAndDate(input: String, minYear: Int, maxYear: Int): Boolean {
    if(validate4Digits(input)) {
        return input.toInt() in minYear..maxYear
    }
    return false
}

fun validate4Digits(input: String) : Boolean {
    return """\d{4}""".toRegex().matches(input)
}
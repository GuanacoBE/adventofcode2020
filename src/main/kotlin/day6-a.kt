fun main(args: Array<String>) {
    /*val input = "abc\n" +
            "\n" +
            "a\n" +
            "b\n" +
            "c\n" +
            "\n" +
            "ab\n" +
            "ac\n" +
            "\n" +
            "a\n" +
            "a\n" +
            "a\n" +
            "a\n" +
            "\n" +
            "b"*/
    val input = {}.javaClass.getResource("day6-input.txt").readText(Charsets.UTF_8)

    val groups = input.split("\n\n")
    var total = 0

    for (group in groups) {
        val cleaned = group.replace("\n", "")
        val uniqueQuestions = mutableSetOf<Char>()

        for (c in cleaned) {
            uniqueQuestions.add(c)
        }
        total += uniqueQuestions.size
    }

    println(total)
}
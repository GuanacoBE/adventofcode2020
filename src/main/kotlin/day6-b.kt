fun main(args: Array<String>) {
    /*val input = "abc\n" +
            "\n" +
            "a\n" +
            "b\n" +
            "c\n" +
            "\n" +
            "ab\n" +
            "ac\n" +
            "\n" +
            "a\n" +
            "a\n" +
            "a\n" +
            "a\n" +
            "\n" +
            "b"*/
    val input = {}.javaClass.getResource("day6-input.txt").readText(Charsets.UTF_8)

    val groups = input.split("\n\n")
    var total = 0

    for (group in groups) {
        var countGroup = 0
        val numberOfPeopleInGroup = group.count { it == '\n' } + 1

        val people = group.split("\n")

        for (c in people[0]) {
            val questionOccurrence = group.count { it == c }
            if(questionOccurrence == numberOfPeopleInGroup) {
                countGroup++
            }
        }
        total += countGroup
    }

    println(total)
}
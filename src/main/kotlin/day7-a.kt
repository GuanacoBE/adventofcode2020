fun main(args: Array<String>) {
    /*val input = "light red bags contain 1 bright white bag, 2 muted yellow bags.\n" +
            "dark orange bags contain 3 bright white bags, 4 muted yellow bags.\n" +
            "bright white bags contain 1 shiny gold bag.\n" +
            "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.\n" +
            "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.\n" +
            "dark olive bags contain 3 faded blue bags, 4 dotted black bags.\n" +
            "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.\n" +
            "faded blue bags contain no other bags.\n" +
            "dotted black bags contain no other bags."*/
    val input = {}.javaClass.getResource("day7-input.txt").readText(Charsets.UTF_8)

    val rules = buildRules(input)

    val searcher = Searcher(rules)
    searcher.isDeepContains()

    println(searcher.count) // 101

}

fun buildRules(input: String): Map<String, List<Pair<String>>> {
    val rules = mutableMapOf<String, List<Pair<String>>>()
    val rulesList = input.split("\n")
    for (rule in rulesList) {
        val cleaned = rule.replace(".", "")
        val nameAndConstraints = cleaned.split("contain")
        val name = nameAndConstraints[0].replace("bags", "").trim()
        rules[name] = parseOneRule(nameAndConstraints[1])
    }
    return rules
}

fun parseOneRule(ruleStr: String): List<Pair<String>> {
    val result = mutableListOf<Pair<String>>()
    val constraints = ruleStr.split(", ")
    if(constraints.size == 1 && constraints[0].trim() == "no other bags") {
        return listOf()
    }
    for (constraint in constraints) {
        val cleaned = constraint.trim()
        result.add(Pair(cleaned[0].toString(), cleaned.drop(2).replace("bags", "").replace("bag", "").trim()))
    }
    return result
}

class Searcher(val rules: Map<String, List<Pair<String>>>) {
    var count = 0

    fun isDeepContains() {
        for (rule in rules) {
            if(isGoldBagInBag(rule.key)) {
                count++
            }
        }
    }

    private fun isGoldBagInBag(color: String): Boolean {
        for (subBag in rules[color]!!) {
            if (subBag.value2.equals("shiny gold", ignoreCase = true)) {
                return true
            }
        }

        for (subBag in rules[color]!!) {
            if (isGoldBagInBag(subBag.value2)) {
                return true
            }
        }

        return false
    }

}

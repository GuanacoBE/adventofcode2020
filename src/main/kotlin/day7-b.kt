fun main(args: Array<String>) {
    /*val input = "shiny gold bags contain 2 dark red bags.\n" +
            "dark red bags contain 2 dark orange bags.\n" +
            "dark orange bags contain 2 dark yellow bags.\n" +
            "dark yellow bags contain 2 dark green bags.\n" +
            "dark green bags contain 2 dark blue bags.\n" +
            "dark blue bags contain 2 dark violet bags.\n" +
            "dark violet bags contain no other bags."*/
    val input = {}.javaClass.getResource("day7-input.txt").readText(Charsets.UTF_8)

    val rules = buildRules(input)

    val counter = Counter(rules)

    println(counter.countBagInShinyGold("shiny gold") - 1) // 108636

}

class Counter(val rules: Map<String, List<Pair<String>>>) {
    fun countBagInShinyGold(color: String): Int {
        val list = rules[color]!!
        var count = 1

        for (pair in list) {
            count += (pair.value1.toInt() * countBagInShinyGold(pair.value2))
        }

        return count
    }

}

import kotlin.Pair

fun main(args: Array<String>) {
    /*val input = "nop +0\n" +
            "acc +1\n" +
            "jmp +4\n" +
            "acc +3\n" +
            "jmp -3\n" +
            "acc -99\n" +
            "acc +1\n" +
            "jmp -4\n" +
            "acc +6"*/
    val input = {}.javaClass.getResource("day8-input.txt").readText(Charsets.UTF_8)

    val instructions = input.split("\n")

    var accumulator = 0
    var position = 0

    val dejaVu = mutableSetOf<Int>()

    while(position < instructions.size) {
        if (dejaVu.contains(position)) {
            break
        }
        val instruction = parseInstruction(instructions[position])
        dejaVu.add(position)
        when (instruction.first) {
            "acc" -> {
                accumulator += instruction.second
                position++
            }
            "jmp" -> {
                position += instruction.second
            }
            "nop" -> position++
        }
    }

    println(accumulator) // 1331

}

fun parseInstruction(str: String): Pair<String, Int> {
    val instruction = str.split(" ")
    return Pair(instruction[0], instruction[1].toInt())
}

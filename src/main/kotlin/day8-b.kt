import kotlin.Pair

fun main(args: Array<String>) {
    /*al input = "nop +0\n" +
            "acc +1\n" +
            "jmp +4\n" +
            "acc +3\n" +
            "jmp -3\n" +
            "acc -99\n" +
            "acc +1\n" +
            "jmp -4\n" +
            "acc +6"*/
    val input = {}.javaClass.getResource("day8-input.txt").readText(Charsets.UTF_8)

    val instructions = input.split("\n").toMutableList()

    var position = 0
    var continueToTry = true
    var oldInstruction = ""
    var programResult: Pair<Int, Boolean>
    do {
        var found = false
        while (!found && position < instructions.size) {
            val instruction = parseInstruction(instructions[position])
            when (instruction.first) {
                "nop" -> {
                    found = true
                    oldInstruction = instructions[position]
                    instructions[position] = oldInstruction.replace("nop", "jmp")
                }
                "jmp" -> {
                    found = true
                    oldInstruction = instructions[position]
                    instructions[position] = oldInstruction.replace("jmp", "nop")
                }
                else -> position++
            }
        }
        programResult = runProgram(instructions)
        if (!programResult.second) {
            continueToTry = false
        } else {
            instructions[position] = oldInstruction
        }
        position++
    } while (continueToTry && position < instructions.size)

    println(programResult)
}

fun runProgram(instructions: List<String>): Pair<Int, Boolean> {
    var accumulator = 0
    var position = 0
    var infinite = false

    val dejaVu = mutableSetOf<Int>()

    while (position < instructions.size) {
        if (dejaVu.contains(position)) {
            infinite = true
            break
        }
        val instruction = parseInstruction(instructions[position])
        dejaVu.add(position)
        when (instruction.first) {
            "acc" -> {
                accumulator += instruction.second
                position++
            }
            "jmp" -> {
                position += instruction.second
            }
            "nop" -> position++
        }
    }
    return Pair(accumulator, infinite)
}

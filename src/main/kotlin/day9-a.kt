fun main(args: Array<String>) {
    val preambleSize = 25
    /*val input = "35\n" +
            "20\n" +
            "15\n" +
            "25\n" +
            "47\n" +
            "40\n" +
            "62\n" +
            "55\n" +
            "65\n" +
            "95\n" +
            "102\n" +
            "117\n" +
            "150\n" +
            "182\n" +
            "127\n" +
            "219\n" +
            "299\n" +
            "277\n" +
            "309\n" +
            "576"*/

    val input = {}.javaClass.getResource("day9-input.txt").readText(Charsets.UTF_8)

    val numbers = input.split("\n").map { it.toLong() }

    for (i in preambleSize+1 until numbers.size) {
        if(!isSumInLastNumber(numbers.subList(i-preambleSize-1, i), numbers[i])){
            println(numbers[i]) // 36845998
        }
    }
}

fun isSumInLastNumber(numbers: List<Long>, sum: Long): Boolean {
    for (number in numbers) {
        val tmp = sum - number
        if (numbers.contains(tmp)) {
            return true
        }
    }
    return false
}

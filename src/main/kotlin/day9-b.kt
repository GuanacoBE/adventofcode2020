fun main(args: Array<String>) {
    val totalSum: Long = 36845998
    //val totalSum:Long = 127
    /*val input = "35\n" +
            "20\n" +
            "15\n" +
            "25\n" +
            "47\n" +
            "40\n" +
            "62\n" +
            "55\n" +
            "65\n" +
            "95\n" +
            "102\n" +
            "117\n" +
            "150\n" +
            "182\n" +
            "127\n" +
            "219\n" +
            "299\n" +
            "277\n" +
            "309\n" +
            "576"*/

    val input = {}.javaClass.getResource("day9-input.txt").readText(Charsets.UTF_8)

    val numbers = input.split("\n").map { it.toLong() }

    for (i in numbers.indices) {
        var max = numbers[i]
        var min = numbers[i]
        var currentSum = numbers[i]
        for (j in i+1 until numbers.size) {
            currentSum += numbers[j]
            if(numbers[j] < min) {
                min = numbers[j]
            }
            if(numbers[j] > max) {
                max = numbers[j]
            }
            if(currentSum == totalSum) {
                println("$min + $max = ${min+max}")
            }
        }
    }
}
